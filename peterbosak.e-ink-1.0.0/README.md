# Anchorage Theme for Visual Studio Code

Anchorage is a Light color theme for Visual Studio Code optimised for JavaScript development.

## Installation

In the command palette (`ctrl/cmd + shift + p`) type Install Extension and search for Anchorage.

## Screenshots

JavaScript

![JavaScript Theme Screenshot](https://github.com/39digits/anchorage-vscode-theme/raw/master/images/example-js.png)

JSX

![JSX Theme Screenshot](https://github.com/39digits/anchorage-vscode-theme/raw/master/images/example-jsx.png)

HTML

![HTML Theme Screenshot](https://github.com/39digits/anchorage-vscode-theme/raw/master/images/example-html.png)

CSS

![CSS Theme Screenshot](https://github.com/39digits/anchorage-vscode-theme/raw/master/images/example-css.png)

## Recommended Extras

I highly recommend installing the
[Latest TypeScript and Javascript Grammar](https://marketplace.visualstudio.com/items?itemName=ms-vscode.typescript-javascript-grammar) extension for much improved JavaScript (and TypeScript) syntax highlighting.

**Enjoy!**
